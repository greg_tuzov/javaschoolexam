package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if(null == x || null == y)
            throw new IllegalArgumentException("x or y is null");

        boolean[] finded = new boolean[x.size()];
        for(int i = 0; i < finded.length; i++) {
            finded[i] = false;
        }

        int k = 0;
        for(int i = 0; i < x.size(); i++) {
            for(; k < y.size(); k++) {
                if(x.get(i).equals(y.get(k))) {
                    finded[i] = true;
                    break;
                }
            }
        }

        for(int i = 0; i < finded.length; i++) {
            if(!finded[i])
                return false;
        }

        return true;
    }
}
