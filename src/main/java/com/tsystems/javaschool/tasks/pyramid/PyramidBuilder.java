package com.tsystems.javaschool.tasks.pyramid;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.util.*;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // Check params
        if(null == inputNumbers)
            throw new CannotBuildPyramidException("Referense is null");
        if(0 == inputNumbers.size())
            throw new CannotBuildPyramidException("List is empty");
        for(Integer elem : inputNumbers)
            if(null == elem)
                throw new CannotBuildPyramidException("List contains null");

        // Check that we can build pyramid
        int w = 0;
        int h = 0;
        long nElemsCounter = 0;
        for(long i = 1; nElemsCounter < inputNumbers.size(); i++) {
            nElemsCounter = nElemsCounter + i;
            w = (int) i;
        }
        h = w;
        if(nElemsCounter != inputNumbers.size())
            throw new CannotBuildPyramidException("Wrong size of the list");


        inputNumbers.sort((n1, n2) -> n1.compareTo(n2));

        if(1 == h) {
            int[][] res1 = new int[1][1];
            res1[0][0] = inputNumbers.get(0);
            return res1;
        }
        LinkedList<Integer> iN = new LinkedList<>(inputNumbers);
        if(2 == h) {
            int[][] res = new int[2][3];
            res[0][1] = iN.pollFirst();
            res[1][0] = iN.pollFirst();     //1
            res[1][2] = iN.pollFirst();     //2
            return res;
        }

        // Build pyramid
        w = 2*w - 1;
        int[][] pyramidArr = new int[h][w];
        LinkedList<Integer> idxInOddLines = new LinkedList<>();
        idxInOddLines.push(0);
        LinkedList<Integer> idxInEvenLines = new LinkedList<>();
        idxInEvenLines.push(1);
        idxInEvenLines.push(-1);

        pyramidArr[0][w/2] = iN.pollFirst();
        pyramidArr[1][w/2-1] = iN.pollFirst();     //1
        pyramidArr[1][w/2+1] = iN.pollFirst();     //2
        for (int i = 3; i <= h; i++) {
                if(i % 2 == 1) {
                    idxInOddLines.addFirst(idxInOddLines.getFirst()-2);
                    idxInOddLines.addLast(idxInOddLines.getLast()+2);
                    for(int idx : idxInOddLines) {
                        pyramidArr[i-1][w/2 + idx] = iN.pollFirst();
                    }
                } else {
                    idxInEvenLines.addFirst(idxInEvenLines.getFirst()-2);
                    idxInEvenLines.addLast(idxInEvenLines.getLast()+2);
                    for(int idx : idxInEvenLines) {
                        pyramidArr[i-1][w/2 + idx] = iN.pollFirst();
                    }
                }
        }
        return pyramidArr;
    }
}
