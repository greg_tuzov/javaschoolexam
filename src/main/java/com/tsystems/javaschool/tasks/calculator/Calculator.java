package com.tsystems.javaschool.tasks.calculator;

import java.util.*;
import java.util.regex.*;

public class Calculator {

    private static double EPSILON = 10e-6;

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        if(null == statement || !checkExpr(statement))
            return null;

        double result = Double.MAX_VALUE;
        try {
            result = postfixEval(infixToPostfix(statement));
        } catch (ArithmeticException ex) {
            return null;
        }

        long intPart = (long) result;
        if(Math.abs(result) - Math.abs(intPart) < EPSILON)
            return Long.toString(intPart);

        return Double.toString(result);
    }

    boolean checkExpr(String statement) {
        // Check if contains invalid characters
        Pattern p = Pattern.compile("[\\+\\-\\*/()0-9\\.]+");
        Matcher m = p.matcher(statement);
        if(!m.matches())
            return false;

        // Check if wrong brackets sequence
        int counter = 0;
        for(char c : statement.toCharArray()) {
            if('(' == c)
                counter++;
            else if(')' == c)
                counter--;
            if(counter < 0)
                return false;
        }
        if(!(0 == counter))
            return false;

        // Check if wrong using of operators and dots
        // Operators
        p = Pattern.compile("[^\\d()][\\+\\-\\*/]");
        Matcher m1 = p.matcher(statement);
        p = Pattern.compile("[\\+\\-\\*/][^\\d()]");
        Matcher m2 = p.matcher(statement);
        // Dots
        p = Pattern.compile("\\D\\.");
        Matcher m3 = p.matcher(statement);
        p = Pattern.compile("\\.\\D");
        Matcher m4 = p.matcher(statement);

        if(m1.find() || m2.find() || m3.find() || m4.find())
            return false;

        // Check if division by zero
        p = Pattern.compile("/0[.[0]+]?]");
        m = p.matcher(statement);
        if(m.find())
            return false;

        // как обходиться с пустой строкой

        return true;
    }

    List<String> infixToPostfix(String statement) {
        Stack<String> opStack = new Stack<>();
        List<String> postfixList = new ArrayList<>();
        Map<String, Integer> prior = new HashMap<>();
        prior.put("(", 0);
        prior.put("+", 1);
        prior.put("-", 1);
        prior.put("*", 2);
        prior.put("/", 2);
        StringTokenizer st = new StringTokenizer(statement, "+-*/()", true);
        List<String> tokens = new ArrayList<>();

        while(st.hasMoreTokens()) {
            tokens.add(st.nextToken());
        }
        Double d = 0.0;
        boolean isNum = true;
        for(String token : tokens) {
            try {
                d = Double.parseDouble(token);
                isNum = true;
            } catch(NumberFormatException ex) {
                isNum = false;
            }

            if(isNum)
                postfixList.add(token);
            if("+-*/".contains(token)) {
                while(!opStack.isEmpty() && prior.get(opStack.peek()) >= prior.get(token)) {
                    postfixList.add(opStack.pop());
                }
                opStack.push(token);
            }
            if("(".equals(token))
                opStack.push(token);
            if(")".equals(token)) {
                String onTopToken = opStack.pop();
                while(!"(".equals(onTopToken)) {
                    postfixList.add(onTopToken);
                    onTopToken = opStack.pop();
                }
            }
        }

        while(!opStack.isEmpty())
            postfixList.add(opStack.pop());

        return postfixList;
    }

    Double postfixEval(List<String> postfixExpr) {
        Stack<Double> operandStack = new Stack<>();
        boolean isNum = true;
        Double d = 0.0;
        for(String token : postfixExpr) {
            try {
                d = Double.parseDouble(token);
                isNum = true;
            } catch(NumberFormatException ex) {
                isNum = false;
            }

            if(isNum) {
                operandStack.push(d);
            } else {
                Double operand2 = operandStack.pop();
                Double operand1 = operandStack.pop();
                if("+".equals(token)) {
                    operandStack.push(operand1 + operand2);
                } else if ("-".equals(token)) {
                    operandStack.push(operand1 - operand2);
                } else if("*".equals(token)) {
                    operandStack.push(operand1 * operand2);
                } else {
                    if(Math.abs(operand2) < EPSILON) {
                        throw new ArithmeticException("Denominator can't be zero");
                    }
                    operandStack.push(operand1 / operand2);
                }
            }
        }
        return operandStack.pop();
    }
}
